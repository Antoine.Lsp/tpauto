import csv

def address() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        adresses = []
        for row in reader:
            adresses.append(row['address'])
        return adresses

def carrosserie() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        carrosserie = []
        for row in reader:
            carrosserie.append(row['carrosserie'])
        return carrosserie

def categorie() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        categorie = []
        for row in reader:
            categorie.append(row['categorie'])
        return categorie

def couleur() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        couleur = []
        for row in reader:
            couleur.append(row['couleur'])
        return couleur

def cylindree() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        cylindree = []
        for row in reader:
            cylindree.append(row['cylindree'])
        return cylindree

def date_immat() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        date_immat = []
        for row in reader:
            date_immat.append(row['date_immat'])
        return date_immat

def denomination() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        denomination = []
        for row in reader:
            denomination.append(row['denomination'])
        return denomination

def energy() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        energy = []
        for row in reader:
            energy.append(row['energy'])
        return energy

def firstname() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        firstname = []
        for row in reader:
            firstname.append(row['firstname'])
        return firstname

def immat() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        immat = []
        for row in reader:
            immat.append(row['immat'])
        return immat

def marque() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        marque = []
        for row in reader:
            marque.append(row['marque'])
        return marque

def name() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        name = []
        for row in reader:
            name.append(row['name'])
        return name

def places() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        places = []
        for row in reader:
            places.append(row['places'])
        return places

def poids() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        poids = []
        for row in reader:
            poids.append(row['poids'])
        return poids

def puissance() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        puissance = []
        for row in reader:
            puissance.append(row['puissance'])
        return puissance

def vin() :
    with open('auto.csv') as csvfile:
        reader=csv.DictReader(csvfile, delimiter='|')
        vin = []
        for row in reader:
            vin.append(row['vin'])
        return vin

with open('auto.csv') as csvfile:
    reader=csv.DictReader(csvfile, delimiter='|')
    type=[]
    variante=[]
    version=[]
    for row in reader:
        toadd = row['type_variante_version'].split(",",3)
        type.append(toadd[0])
        variante.append(toadd[1])
        version.append(toadd[2])

adresse_titulaire=address()
nom=firstname()
prenom=name()
immatriculation=immat()
date_immatriculation=date_immat()
vin=vin()
marque=marque()
denomination_commerciale=denomination()
couleur=couleur()
carroserie=carrosserie()
categorie=categorie()
cylindree=cylindree()
energie=energy()
places=places()
poids=poids()
puissance=puissance()
    

with open('New_votre_fichier.csv', 'w', newline='') as csvfile:
    fieldnames = ['addresse_titulaire','nom','prenom','immatriculation','date_immatriculation','vin','marque','denomination_commerciale','couleur','carroserie','categorie','cylindree','energie','places','poids','puissance','type','variante','version']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for ruw in range(0,200):
        writer.writerow({'addresse_titulaire': adresse_titulaire[ruw],'nom': nom[ruw],'prenom': prenom[ruw],'immatriculation':immatriculation[ruw],'date_immatriculation':date_immatriculation[ruw],'vin':vin[ruw],'marque':marque[ruw],'denomination_commerciale':denomination_commerciale[ruw],'couleur':couleur[ruw],'carroserie':carroserie[ruw],'categorie':categorie[ruw],'cylindree':cylindree[ruw],'energie':energie[ruw],'places':places[ruw],'poids':poids[ruw],'puissance':puissance[ruw],'type':type[ruw],'variante':variante[ruw],'version':version[ruw]})


with open('New_votre_fichier.csv') as csvfile:
        reader=csv.reader(csvfile, delimiter=',')
        for row in reader:
            print(row)
